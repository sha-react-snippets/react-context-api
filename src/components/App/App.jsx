/* eslint-disable react/no-unused-state */

import React, { Component } from 'react';
import { ThemeContext, themes } from '../../contexts/themes';

import Container from '../Container';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      theme: themes.light,
      toggleTheme: this.toggleTheme,
    };
  }

  toggleTheme = () => {
    this.setState((state) => ({
      theme:
        state.theme === themes.light
          ? themes.dark
          : themes.light,
    }));
  }

  render() {
    return (
      <ThemeContext.Provider value={this.state}>
        <Container />
      </ThemeContext.Provider>
    );
  }
}

export default App;
