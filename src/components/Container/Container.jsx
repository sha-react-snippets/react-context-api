import React, { Component } from 'react';
import { ThemeContext } from '../../contexts/themes';

class Content extends Component {
  render() {
    const { theme, toggleTheme } = this.context;

    return (
      <button
        style={{ backgroundColor: theme.button.backgroundColor, color: theme.button.color }}
        onClick={toggleTheme}
        type="button"
      >
        Toggle Theme
      </button>
    );
  }
}

Content.contextType = ThemeContext;

export default Content;
